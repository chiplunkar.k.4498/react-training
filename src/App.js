import logo from "./logo.svg";
import "./App.css";
import Calculate from "./Components/Calculate";

function App() {
  return <Calculate />;
}

export default App;
