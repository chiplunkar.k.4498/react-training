import React, { useState } from "react";

let Calculate = () => {
  const [num1, setNum1] = useState("");
  const [num2, setNum2] = useState("");
  let result = 0;
  let [history, setHistory] = useState({});

  const handleClickAdd = (e) => {
    e.preventDefault();

    result = Number(num1) + Number(num2);
    document.getElementById("res").innerHTML = "Result : " + result;
    history[num1 + "+" + num2] = result;
    console.log(history);
  };
  const handleClickSub = (e) => {
    e.preventDefault();

    result = Number(num1) - Number(num2);
    document.getElementById("res").innerHTML = "Result : " + result;
    history[num1 + "-" + num2] = result;
    console.log(history);
  };
  const handleClickMul = (e) => {
    e.preventDefault();

    result = Number(num1) * Number(num2);
    document.getElementById("res").innerHTML = "Result : " + result;
    history[num1 + "*" + num2] = result;
    console.log(history);
  };
  const handleClickDiv = (e) => {
    e.preventDefault();

    result = Number(num1) / Number(num2);
    document.getElementById("res").innerHTML = "Result : " + result;
    history[num1 + "/" + num2] = result;
    console.log(history);
  };
  const displayHistory = (e) => {
    e.preventDefault();
    console.log("inside display history");
    /*for (let element in history) {
      console.log(element);
      document.getElementById("show").innerHTML =
        element + " : " + history[element];
    }*/
    let display = JSON.stringify(history);
    document.getElementById("show").innerHTML = display;
  };

  return (
    <form>
      {console.log("rendered")}
      <label htmlFor="firstNo">Enter First Number : </label>
      <input
        type="text"
        id="firstNo"
        name="firstNo"
        value={num1}
        onChange={(e) => setNum1(e.target.value)}
      />
      <br />
      <br />
      <label htmlFor="secondNo">Enter Second Number : </label>
      <input
        type="text"
        id="secondNo"
        name="secondNo"
        value={num2}
        onChange={(e) => setNum2(e.target.value)}
      />
      <br />
      <br />
      <label htmlFor="resultLabel" id="res"></label>
      <br />
      <br />
      <button type="submit" onClick={handleClickAdd}>
        Add
      </button>
      <button type="submit" onClick={handleClickSub}>
        Subtract
      </button>
      <button type="submit" onClick={handleClickMul}>
        Multiply
      </button>
      <button type="submit" onClick={handleClickDiv}>
        Divide
      </button>
      <button onClick={displayHistory}>History</button>
      <br />
      <br />
      <p id="show"></p>
    </form>
  );
};

export default Calculate;
